﻿using AutoMapper;
using CustomerAPI.Entities.DTOs;
using CustomerAPI.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerAPI
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerDto>();

            CreateMap<CustomerCreationDto, Customer>();

            CreateMap<CustomerUpdateDto, Customer>();
        }
    }
}

