﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerAPI.Contracts
{
    public interface IRepositoryWrapper 
    { 
        ICustomerRepository Customer { get; } 
        void Save(); 
    }
}
