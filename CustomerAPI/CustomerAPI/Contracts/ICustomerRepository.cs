﻿using CustomerAPI.Entities.Models;
using System;
using System.Collections.Generic;

namespace CustomerAPI.Contracts
{
    public interface ICustomerRepository : IBaseRepository<Customer>
    {
        IEnumerable<Customer> GetAllCustomers();
        Customer GetCustomerById(Guid customerId);
        Customer FindCustomerByName(string partialName);
        void CreateCustomer(Customer customer);
        void UpdateCustomer(Customer customer);
        void DeleteCustomer(Customer customer);
        void Save();
    }
}
