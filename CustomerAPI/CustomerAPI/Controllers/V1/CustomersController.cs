﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CustomerAPI.Contracts;
using CustomerAPI.Entities.DTOs;
using CustomerAPI.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CustomerAPI.Controllers.V1
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomerRepository _repository;
        private readonly ILogger<CustomersController> _logger;
        private IMapper _mapper;

        public CustomersController(ILogger<CustomersController> logger, ICustomerRepository repository, IMapper mapper)
        {
            _logger = logger;
            _repository = repository;
            _mapper = mapper;
        }

        // Get api/v1/customers
        [HttpGet]
        public ActionResult Get()
        {
            try
            {
                var customers = _repository.GetAllCustomers();
                _logger.LogInformation($"Returned all customers from database.");

                var customersResult = _mapper.Map<IEnumerable<CustomerDto>>(customers);
                return Ok(customersResult);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetAll action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET api/v1/customers/ab2bd817-98cd-4cf3-a80a-53ea0cd9c200
        [HttpGet("{id}", Name = "CustomerById")]
        public IActionResult CustomerById(Guid id)
        {
            try
            {
                var customer = _repository.GetCustomerById(id);
                if (customer == null)
                {
                    _logger.LogError($"Customer with id: {id}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInformation($"Returned customer with id: {id}");

                    var customerResult = _mapper.Map<CustomerDto>(customer);
                    return Ok(customerResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCustomerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // FindCustomerByName api/v1/customers/FindCustomerByName/nam
        [HttpGet("FindCustomerByName/{partialName}", Name = "FindCustomerByName")]
        public IActionResult FindCustomerByName(string partialName)
        {
            try
            {
                var customer = _repository.FindCustomerByName(partialName);
                if (customer == null)
                {
                    _logger.LogError($"Customer with name: {partialName}, hasn't been found in db.");
                    return NotFound();
                }
                else
                {
                    _logger.LogInformation($"Returned customer with partialName: {partialName}");

                    var customerResult = _mapper.Map<CustomerDto>(customer);
                    return Ok(customerResult);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside GetCustomerById action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // POST api/v1/customers
        [HttpPost]
        public IActionResult Post([FromBody] CustomerCreationDto customer)
        {
            try
            {
                if (customer == null)
                {
                    _logger.LogError("Customer object sent from client is null.");
                    return BadRequest("Customer object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid customer object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var customerEntity = _mapper.Map<Customer>(customer);

                _repository.CreateCustomer(customerEntity);
                _repository.Save();

                var createdCustomer = _mapper.Map<CustomerDto>(customerEntity);

                return CreatedAtRoute("CustomerById", new { id = createdCustomer.Id }, createdCustomer);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside CreateCustomer action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // PUT api/v1/customers/ab2bd817-98cd-4cf3-a80a-53ea0cd9c200
        [HttpPut("{id}")]
        public IActionResult Put(Guid id, [FromBody]CustomerUpdateDto customer)
        {
            try
            {
                if (customer == null)
                {
                    _logger.LogError("Customer object sent from client is null.");
                    return BadRequest("Customer object is null");
                }

                if (!ModelState.IsValid)
                {
                    _logger.LogError("Invalid customer object sent from client.");
                    return BadRequest("Invalid model object");
                }

                var customerEntity = _repository.GetCustomerById(id);
                if (customerEntity == null)
                {
                    _logger.LogError($"Customer with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _mapper.Map(customer, customerEntity);

                _repository.UpdateCustomer(customerEntity);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside UpdateCustomer action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // DELETE api/v1/customers/ab2bd817-98cd-4cf3-a80a-53ea0cd9c200
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
            try
            {
                var customer = _repository.GetCustomerById(id);
                if (customer == null)
                {
                    _logger.LogError($"Customer with id: {id}, hasn't been found in db.");
                    return NotFound();
                }

                _repository.DeleteCustomer(customer);
                _repository.Save();

                return NoContent();
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong inside DeleteCustomer action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}
