﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerAPI.Configs
{
    public class SwaggerConfigurations
    {
        public string Description { get; set; }
        public string UIEndpoint { get; set; }
    }
}
