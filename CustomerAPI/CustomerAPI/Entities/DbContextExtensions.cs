﻿using System;
using CustomerAPI.Entities.Models;

namespace CustomerAPI.Entities
{
    public static class DbContextExtensions
    {
        public static void Seed(this DataContext dbContext)
        {
            // Add entities for DbContext instance

            dbContext.Customers.Add(new Customer()
            {
                Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                FirstName = "Nauman",
                LastName = "Shaukat",
                DateOfBirth = new DateTime(1983, 01, 10)
            });
            dbContext.Customers.Add(new Customer()
            {
                Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                FirstName = "Faris",
                LastName = "Ahmed",
                DateOfBirth = new DateTime(2019, 02, 28)
            });
            dbContext.Customers.Add(new Customer()
            {
                Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
                FirstName = "Waseem",
                LastName = "Chihsti",
                DateOfBirth = new DateTime(1983, 05, 27)
            });

            dbContext.SaveChanges();
        }
    }
}