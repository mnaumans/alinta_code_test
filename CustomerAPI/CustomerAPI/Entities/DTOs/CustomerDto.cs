﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerAPI.Entities.DTOs
{
    public class CustomerDto
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
     
    }
}
