﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CustomerAPI.Entities.DTOs
{
    public class CustomerUpdateDto
    {
        [Required(ErrorMessage = "First Name is required")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Last Name is required")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Date of birth is required")]
        public DateTime DateOfBirth { get; set; }
    }
}
