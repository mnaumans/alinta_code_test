﻿using CustomerAPI.Contracts;
using CustomerAPI.Entities.Models;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerAPI.Entities.Repos
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DataContext dbContext)
            : base(dbContext)
        {
        }

        public IEnumerable<Customer> GetAllCustomers()
        {
            return FindAll()
                .OrderBy(cst => cst.FirstName).ThenBy(cst => cst.LastName)
                .ToList();
        }

        public Customer GetCustomerById(Guid customerId)
        {
            return FindByCondition(customer => customer.Id.Equals(customerId))
                .FirstOrDefault();
        }

        public Customer FindCustomerByName(string partialName)
        {
            return FindByCondition(customer => customer.FirstName.ToLower().Contains(partialName.ToLower()) || customer.LastName.ToLower().Contains(partialName.ToLower()))
                .FirstOrDefault();
        }

        public void CreateCustomer(Customer customer)
        {
            Create(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            Update(customer);
        }

        public void DeleteCustomer(Customer customer)
        {
            Delete(customer);
        }

    }
}

