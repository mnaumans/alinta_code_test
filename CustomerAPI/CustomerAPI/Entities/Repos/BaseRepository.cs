﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using CustomerAPI.Entities;
using CustomerAPI.Contracts;

namespace Repository
{
    public abstract class BaseRepository<T> : IBaseRepository<T> where T : class
    {
        protected DataContext DBContext { get; set; }
        public BaseRepository(DataContext dbContext)
        {
            DBContext = dbContext;
        }

        public IQueryable<T> FindAll()
        {
            return DBContext.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return DBContext.Set<T>().Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            DBContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            DBContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            DBContext.Set<T>().Remove(entity);
        }

        public void Save()
        {
            DBContext.SaveChanges();
        }

    }
}
