﻿using CustomerAPI.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerAPI.Entities.Repos
{
    public class RepositoryWrapper : IRepositoryWrapper
    {
        private DataContext _dbContext;
        private ICustomerRepository _customer;
        public ICustomerRepository Customer
        {
            get
            {
                if (_customer == null)
                {
                    _customer = new CustomerRepository(_dbContext);
                }
                return _customer;
            }
        }

        public RepositoryWrapper(DataContext dataContext)
        {
            _dbContext = dataContext;
        }

        public void Save()
        {
            _dbContext.SaveChanges();
        }
    }
}
