﻿using Xunit;
using CustomerAPI.Controllers.V1;
using System;
using System.Collections.Generic;
using System.Text;
using Moq;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using CustomerAPI.Entities;
using AutoMapper;
using CustomerAPI.Contracts;
using Microsoft.AspNetCore.Mvc;
using CustomerAPI.Entities.DTOs;
using Microsoft.AspNetCore.Http;
using AutoMapper.Configuration;
using CustomerAPI;
using CustomerAPI.Entities.Models;
using System.Net;

namespace CustomerAPITest.Controllers.V1.Tests
{
    public class CustomersControllerTests
    {
        private Mock<ICustomerRepository> _repositoryMock;
        private Mock<IMapper> _mapperMock;
        private readonly Mock<ILogger<CustomersController>> _loggerMock;

        public CustomersControllerTests()
        {
            _repositoryMock = new Mock<ICustomerRepository>();
            _mapperMock = new Mock<IMapper>();
            _loggerMock = new Mock<ILogger<CustomersController>>();
        }

        [Fact()]
        public void Get_StatusCode_Success()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            //Act
            var actionResult = fakeCustomersController.Get();

            //Assert
            Assert.True((int)HttpStatusCode.OK == (actionResult as ObjectResult).StatusCode);
        }

        [Fact]
        public void GetById_UnknownGuidPassed_ReturnsNotFoundResult()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var notFoundResult = fakeCustomersController.CustomerById(Guid.NewGuid());

            // Assert
            Assert.IsType<NotFoundResult>(notFoundResult);
        }

        [Fact]
        public void GetById_ExistingIdPassed_ReturnsOkResult()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var customers = GetFakeData().Find(x => x.Id.Equals(testGuid));
            _repositoryMock.Setup(x => x.GetCustomerById(testGuid)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var okResult = fakeCustomersController.CustomerById(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<OkObjectResult>(okResult);
        }

        [Fact]
        public void GetById_ExistingGuidPassed_ReturnsRightItem()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var customers = GetFakeData().Find(x => x.Id.Equals(testGuid));
            _repositoryMock.Setup(x => x.GetCustomerById(testGuid)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var okResult = fakeCustomersController.CustomerById(testGuid) as OkObjectResult;

            // Assert
            Assert.IsType<CustomerDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as CustomerDto).Id);
        }

        [Fact()]
        public void FindCustomerByName_ReturnsOkResult()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var partialName = "Naum";
            var customers = GetFakeData().Find(
                customer => customer.FirstName.ToLower().Contains(partialName.ToLower()) 
                || customer.LastName.ToLower().Contains(partialName.ToLower())); 
            _repositoryMock.Setup(x => x.FindCustomerByName(partialName)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var okResult = fakeCustomersController.FindCustomerByName(partialName) as OkObjectResult;

            // Assert
            Assert.IsType<CustomerDto>(okResult.Value);
        }

        [Fact()]
        public void FindCustomerByName_ReturnsCorrectCustomer()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var partialName = "Naum";
            var customers = GetFakeData().Find(
                customer => customer.FirstName.ToLower().Contains(partialName.ToLower())
                || customer.LastName.ToLower().Contains(partialName.ToLower()));
            _repositoryMock.Setup(x => x.FindCustomerByName(partialName)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var okResult = fakeCustomersController.FindCustomerByName(partialName) as OkObjectResult;

            // Assert
            Assert.IsType<CustomerDto>(okResult.Value);
            Assert.Equal(testGuid, (okResult.Value as CustomerDto).Id);
        }

        [Fact()]
        public void PostTest()
        {
            // Arrange
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);
            var testGuid = Guid.NewGuid();

            // Act
            IActionResult actionResult = fakeCustomersController.Post(new CustomerCreationDto()
            {
                FirstName = "Nauman",
                LastName = "Shaukat",
                DateOfBirth = DateTime.Now
            });

            var okResult = actionResult as CreatedAtRouteResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal("CustomerById", okResult.RouteName);
        }

        [Fact]
        public void Put_Customer()
        {
            // Arrange   
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var customers = GetFakeData().Find(x => x.Id.Equals(testGuid));
            _repositoryMock.Setup(x => x.GetCustomerById(testGuid)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            CustomerUpdateDto customer = new CustomerUpdateDto();
            customer.FirstName = "Naums";
            customer.LastName = "Chomsky";
            customer.DateOfBirth = new DateTime(1983, 01, 10);
            var putResult = fakeCustomersController.Put(testGuid, customer) as NoContentResult;
            
            // Assert
            Assert.True((int)HttpStatusCode.NoContent == putResult.StatusCode);
        }

        [Fact]
        public void Delete_Category_Returns_NoContentStatusCode()
        {
            // Arrange   
            var mappings = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MappingProfile());
            });
            var _mapper = mappings.CreateMapper();
            var testGuid = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200");
            var customers = GetFakeData().Find(x => x.Id.Equals(testGuid));
            _repositoryMock.Setup(x => x.GetCustomerById(testGuid)).Returns(customers);
            var fakeCustomersController = new CustomersController(_loggerMock.Object, _repositoryMock.Object, _mapper);

            // Act
            var deleteResult = fakeCustomersController.Delete(testGuid) as NoContentResult;

            // Assert
            Assert.True((int)HttpStatusCode.NoContent == deleteResult.StatusCode);

        }
        private List<Customer> GetFakeData()
        {
            return new List<Customer>(){new Customer()
            {
                Id = new Guid("ab2bd817-98cd-4cf3-a80a-53ea0cd9c200"),
                FirstName = "Nauman",
                LastName = "Shaukat",
                DateOfBirth = new DateTime(1983, 01, 10)
            },
            new Customer()
            {
                Id = new Guid("815accac-fd5b-478a-a9d6-f171a2f6ae7f"),
                FirstName = "Faris",
                LastName = "Ahmed",
                DateOfBirth = new DateTime(2019, 02, 28)
            },
            new Customer()
            {
                Id = new Guid("33704c4a-5b87-464c-bfb6-51971b4d18ad"),
                FirstName = "Waseem",
                LastName = "Chihsti",
                DateOfBirth = new DateTime(1983, 05, 27)
            } };
        }
    }
}